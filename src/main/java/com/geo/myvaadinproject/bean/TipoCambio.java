package com.geo.myvaadinproject.bean;

public class TipoCambio {

    private int ran_hora;
    private int ran_ingresos;
    private int ran_ticket;
    private Integer spread;
    private String territorio;
    private String tip_cliente;

    public int getRan_hora() {
        return ran_hora;
    }

    public void setRan_hora(int ran_hora) {
        this.ran_hora = ran_hora;
    }

    public int getRan_ingresos() {
        return ran_ingresos;
    }

    public void setRan_ingresos(int ran_ingresos) {
        this.ran_ingresos = ran_ingresos;
    }

    public int getRan_ticket() {
        return ran_ticket;
    }

    public void setRan_ticket(int ran_ticket) {
        this.ran_ticket = ran_ticket;
    }

    public Integer getSpread() {
        return spread;
    }

    public void setSpread(Integer spread) {
        this.spread = spread;
    }

    public String getTerritorio() {
        return territorio;
    }

    public void setTerritorio(String territorio) {
        this.territorio = territorio;
    }

    public String getTip_cliente() {
        return tip_cliente;
    }

    public void setTip_cliente(String tip_cliente) {
        this.tip_cliente = tip_cliente;
    }

}
