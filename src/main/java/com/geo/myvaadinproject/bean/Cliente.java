package com.geo.myvaadinproject.bean;

public class Cliente {
    private String dni;
    private int ingreso;
    private String tipoCliente;
    private String ticket;
    private String territorio;
    private int spread;
    private double tipoCambioCompra;
    private double tipoCambioVenta;

    public Cliente() {
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public String getTicket() {
        return ticket;
    }

    public void setTicket(String ticket) {
        this.ticket = ticket;
    }

    public String getTerritorio() {
        return territorio;
    }

    public void setTerritorio(String territorio) {
        this.territorio = territorio;
    }

    public int getIngreso() {
        return ingreso;
    }

    public void setIngreso(int ingreso) {
        this.ingreso = ingreso;
    }

    public String getTipoCliente() {
        return tipoCliente;
    }

    public void setTipoCliente(String tipoCliente) {
        this.tipoCliente = tipoCliente;
    }

    public int getSpread() {
        return spread;
    }

    public void setSpread(int spread) {
        this.spread = spread;
    }

    public double getTipoCambioCompra() {
        return tipoCambioCompra;
    }

    public void setTipoCambioCompra(double tipoCambioCompra) {
        this.tipoCambioCompra = tipoCambioCompra;
    }

    public double getTipoCambioVenta() {
        return tipoCambioVenta;
    }

    public void setTipoCambioVenta(double tipoCambioVenta) {
        this.tipoCambioVenta = tipoCambioVenta;
    }

    @Override
    public String toString() {
        return "Cliente:dni:" + getDni() + ",ticket:" + getTicket() + ",territorio:" + getTerritorio() +
                ",ingreso=" + getIngreso() + ",tipoCliente" + getTipoCliente();
    }
}
