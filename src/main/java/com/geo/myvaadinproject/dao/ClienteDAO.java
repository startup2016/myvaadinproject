package com.geo.myvaadinproject.dao;

import com.geo.myvaadinproject.bean.Cliente;
import com.vaadin.spring.annotation.SpringComponent;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

@SpringComponent
public class ClienteDAO {

    public Map<String,Cliente> readData() throws IOException {
        int count = 0;
        String file = "/DNI-sueldo-tipoCliente.csv";

        InputStream in = this.getClass().getResourceAsStream(file);

        HashMap<String,Cliente> content = new HashMap<String,Cliente>();
        Cliente cli;
        try (BufferedReader br = new BufferedReader(new InputStreamReader(in,"UTF-8"))) {
            String line = "";
            while ((line = br.readLine()) != null) {
                String[] s = line.split(",");
                cli = new Cliente();
                cli.setDni(s[0]);
                cli.setIngreso(Integer.parseInt(s[1]));
                cli.setTipoCliente(s[2]);
                content.put(cli.getDni(),cli);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return content;
    }

    public Cliente getCliente(Cliente cli){

        try {
            Map<String,Cliente> lista = new ClienteDAO().readData();

           Cliente aux = lista.get(cli.getDni());
           cli.setIngreso(aux.getIngreso());
           cli.setTipoCliente(aux.getTipoCliente());

        } catch (IOException e) {
            e.printStackTrace();
        }
        return cli;
    }


//    public static void main(String[] args) {
//        try {
//            Map<String,Cliente> lista = new ClienteDAO().readData();
//            System.out.println("size:"+lista.size());
//            Cliente cli = lista.get("850391");
//            System.out.println("cli:"+cli.getDni());
//            System.out.println("cli:"+cli.getIngreso());
//            System.out.println("cli:"+cli.getTipoCliente());
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//    }
}
