package com.geo.myvaadinproject.ui;

import com.geo.myvaadinproject.bean.Cliente;
import com.geo.myvaadinproject.bean.TipoCambio;
import com.geo.myvaadinproject.service.IClienteService;
import com.vaadin.annotations.Theme;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.server.VaadinRequest;
import com.vaadin.spring.annotation.EnableVaadin;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.spring.server.SpringVaadinServlet;
import com.vaadin.ui.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.context.ContextLoaderListener;

import javax.servlet.annotation.WebListener;
import javax.servlet.annotation.WebServlet;

/**
 * This UI is the application entry point. A UI may either represent a browser window
 * (or tab) or some part of an HTML page where a Vaadin application is embedded.
 * <p>
 * The UI is initialized using {@link #init(VaadinRequest)}. This method is intended to be
 * overridden to add component to the user interface and initialize non-component functionality.
 */
@Theme("mytheme")
@SpringUI
public class MyUI extends UI {

    @Autowired
    IClienteService service;

    Cliente cliente;

    HorizontalLayout hlayout = new HorizontalLayout();
    VerticalLayout vlayout_requ = new VerticalLayout();
    VerticalLayout vlayout_resp = new VerticalLayout();

    TextField dni, ticket;
    ComboBox<String> territorio;
    Button button;
    Label mensaje;

    Label ingreso, tipoCliente;
    TextField spread, tipoCambioCompra, tipoCambioVenta;

    @Override
    protected void init(VaadinRequest vaadinRequest) {
        dni = new TextField();
        dni.setCaption("DNI:");

        ticket = new TextField();
        ticket.setCaption("Ticket:");

        territorio = new ComboBox<>("Territorio:");
        territorio.setItems("Lima Centro", "Norte", "Sur", "Oriente");
        territorio.addValueChangeListener(event -> {
            if (event.getSource().isEmpty()) {
                System.out.println("No seleccionó nada");
            } else {
                System.out.println("seleccionó:" + event.getValue());
            }
        });

        mensaje = new Label();

        button = new Button("Calcular");
        button.addClickListener(e -> {
            procesarCliente();
        });

        ingreso = new Label();
        tipoCliente = new Label();

        spread = new TextField();
        spread.setCaption("Spread");
        spread.setEnabled(false);

        tipoCambioCompra = new TextField();
        tipoCambioCompra.setCaption("tipo cambio compra");
        tipoCambioCompra.setEnabled(false);

        tipoCambioVenta = new TextField();
        tipoCambioVenta.setCaption("tipo cambio venta");
        tipoCambioVenta.setEnabled(false);

        vlayout_requ.addComponents(dni, ticket, territorio, button, mensaje);

        vlayout_resp.addComponents(spread);
        vlayout_resp.setVisible(false);
        hlayout.addComponents(vlayout_requ, vlayout_resp);

        setContent(hlayout);
    }


    private void procesarCliente() {
        cliente = new Cliente();
        cliente.setDni(dni.getValue());
        cliente.setTicket(ticket.getValue());
        cliente.setTerritorio(territorio.getValue());
        System.out.println(cliente);
        TipoCambio tp = service.calcularTipoCambio(cliente);

        System.out.println("ingreso: " + cliente.getIngreso());
        System.out.println("tipo cliente: " + cliente.getTipoCliente());
        spread.setValue(String.valueOf(tp.getSpread()));
        vlayout_resp.setVisible(true);
    }

    @WebServlet(value = "/*", name = "MyUIServlet", asyncSupported = true)
    @VaadinServletConfiguration(ui = MyUI.class, productionMode = false)
    public static class MyUIServlet extends SpringVaadinServlet {
    }

    @WebListener
    public static class MyContextLoaderListener extends ContextLoaderListener {

    }

    @Configuration
    @EnableVaadin
    public static class MyConfiguration {
    }

}
