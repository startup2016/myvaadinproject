package com.geo.myvaadinproject.service;

import com.geo.myvaadinproject.bean.Cliente;
import com.geo.myvaadinproject.bean.TipoCambio;

public interface IClienteService {
    TipoCambio calcularTipoCambio(Cliente cliente);
}
