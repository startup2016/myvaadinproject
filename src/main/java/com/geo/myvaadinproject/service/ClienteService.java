package com.geo.myvaadinproject.service;

import com.geo.myvaadinproject.dao.ClienteDAO;
import com.vaadin.spring.annotation.SpringComponent;
import org.kie.api.KieServices;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.rule.FactHandle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.geo.myvaadinproject.bean.Cliente;
import com.geo.myvaadinproject.bean.TipoCambio;

import java.util.Calendar;
import java.util.Date;

@SpringComponent
public class ClienteService implements IClienteService {

    @Autowired
    ClienteDAO dao;

    public TipoCambio calcularTipoCambio(Cliente cliente) {
        cliente = dao.getCliente(cliente);
        System.out.println(cliente);
        return ejecutarReglas(cliente);
    }

    private TipoCambio ejecutarReglas(Cliente cliente) {
        TipoCambio tipoCambio = new TipoCambio();
        try {
            KieServices ks = KieServices.Factory.get();
            KieContainer kContainer = ks.getKieClasspathContainer();
            KieSession kSession = kContainer.newKieSession("ksession-rule");

            tipoCambio = new TipoCambio();
            tipoCambio.setRan_ingresos(cliente.getIngreso());
            tipoCambio.setRan_hora(obtenerHora());
            tipoCambio.setTerritorio(cliente.getTerritorio());
            tipoCambio.setRan_ticket(Integer.parseInt(cliente.getTicket()));
            tipoCambio.setTip_cliente(cliente.getTipoCliente());

            FactHandle fact1 = kSession.insert(tipoCambio);
            kSession.fireAllRules();

        } catch (Throwable t) {
            t.printStackTrace();
        }
        return tipoCambio;
    }


    private int obtenerHora(){
        return Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
    }

}
